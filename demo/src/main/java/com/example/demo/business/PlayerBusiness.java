package com.example.demo.business;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.MockData;
import com.example.demo.Player;

public class PlayerBusiness {

	@Autowired
	private MockData mockData;

	public Player giveIdToNewPlayer(Player player) {
		// int playersListSize = mockData.getPlayers().size();
		// player.setId(mockData.getPlayers().get(playersListSize-1).getId()+1);
		player.setId(getFirstAvailableId());
		organizeListOfPlayers();
		return player;
	}

	public void organizeListOfPlayers() {
		for (int i = 0; i < mockData.getPlayers().size(); i++) {
			mockData.getPlayers().get(i).setId(i);
		}
	}

	private int getFirstAvailableId() {
		int id = 0;
		for (int i = 0; i < mockData.getPlayers().size(); i++) {
			if (i + 1 != mockData.getPlayers().get(i).getId()) {
				id = i + 1;
			}
		}
		return id;
	}

}
