package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class MockData {

	private List<Player> players;

	public MockData() {
		players = new ArrayList<>();
		fillData();

	}

	private void fillData() {
		players.add(new Player("Cristiano Ronaldo", 1));
		players.add(new Player("Eusébio", 2));
		players.add(new Player("João Felix", 3));
		players.add(new Player("Eduardo", 4));
		players.add(new Player("Pizzi", 5));
		players.add(new Player("Bruno Fernandes", 6));
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	@Override
	public String toString() {
		return "MockData [players=" + players + "]";
	}

}
