package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.MockData;
import com.example.demo.Player;
import com.example.demo.business.PlayerBusiness;

public class PlayerService {

	@Autowired
	private MockData mockData;

	@Autowired
	private PlayerBusiness business;

	public String home() {
		return "Benfica";
	}

	public List<Player> getPlayers() {
		return mockData.getPlayers();
	}

	public String getPlayer(int i) {
		return mockData.getPlayers().stream().filter(player -> player.getId() == i).collect(Collectors.toList()).get(0)
				.getName();
	}

	public List<Player> addPlayer(Player player) {
		mockData.getPlayers().add(business.giveIdToNewPlayer(player));
		business.organizeListOfPlayers();
		return mockData.getPlayers();
	}

	public List<Player> updatePlayer(Player player) {
		mockData.getPlayers().forEach(playerInSystem -> {
			if (playerInSystem.getId() == player.getId()) {
				playerInSystem.setName(player.getName());
			}
		});
		return mockData.getPlayers();
	}

	public List<Player> deletePlayer(Player player) {

		for (int i = 0; i < mockData.getPlayers().size(); i++) {
			if (mockData.getPlayers().get(i).getId() == player.getId()) {
				mockData.getPlayers().remove(i);
			}
		}
		business.organizeListOfPlayers();
		return mockData.getPlayers();
	}
}
