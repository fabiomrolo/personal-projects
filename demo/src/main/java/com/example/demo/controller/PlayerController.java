package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Player;
import com.example.demo.service.PlayerService;

@RestController
public class PlayerController {
		
	@Autowired
	private PlayerService playerService;

	@RequestMapping("/")
	String home() {
		return playerService.home();
	}

	@GetMapping("/players")
	List<Player> getPlayers() {
		return playerService.getPlayers();
	}

	@GetMapping("/player/{i}")
	String getPlayer(@PathVariable int i) {
		return playerService.getPlayer(i);
	}

	@PostMapping("/player")
	List<Player> addPlayer(@RequestBody Player player) {
		return playerService.addPlayer(player);
	}

	@PutMapping("/player")
	List<Player> updatePlayer(@RequestBody Player player) {
		return playerService.updatePlayer(player);
	}

	@DeleteMapping("/player")
	List<Player> deletePlayer(@RequestBody Player player) {
		return playerService.deletePlayer(player);
	}
}
