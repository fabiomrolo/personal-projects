package com.example.demo.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.example.demo.MockData;
import com.example.demo.business.PlayerBusiness;
import com.example.demo.controller.PlayerController;
import com.example.demo.service.PlayerService;

@SpringBootApplication
@ComponentScan(basePackageClasses = PlayerController.class)
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public MockData getMockData() {
		return new MockData();
	}

	@Bean
	public PlayerBusiness getPlayerBusiness() {
		return new PlayerBusiness();
	}

	@Bean
	public PlayerService getPlayerService() {
		return new PlayerService();
	}
}
